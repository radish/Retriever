import re
import string


def clean_text(text: str) -> str:
    return re.sub(' +', ' ', text.translate(str.maketrans({key: None for key in string.punctuation})).lower())
