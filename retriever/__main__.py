from typing import List

import click
import numpy as np
import redis
import time

from retriever.database import CollectionManager
from retriever.distributed import distribute
from retriever.loading import load_cran_documents, load_cran_queries, load_cran_qrels
from retriever.model import CLSMNet
from retriever.processing import extract_words_ngrams, generate_word_vector_map, get_words_ngrams_vectors


@click.group()
def cli():
    pass


@cli.command()
@click.argument('collection_name', type=str)
@click.argument('path', type=click.Path(exists=True, file_okay=True, dir_okay=False))
def index(collection_name: str, path: str):
    collection = CollectionManager(collection_name)
    documents = load_cran_documents(path)
    for document in documents:
        collection.add_document(document)


@cli.command()
@click.argument('collection_name', type=str)
@click.argument('all_queries_path', type=click.Path(exists=True, file_okay=True, dir_okay=False))
@click.argument('train_queries_path', type=click.Path(exists=True, file_okay=True, dir_okay=False))
@click.argument('validation_queries_path', type=click.Path(exists=True, file_okay=True, dir_okay=False))
@click.argument('qrels_path', type=click.Path(exists=True, file_okay=True, dir_okay=False))
@click.argument('model_path', type=click.Path(exists=False, dir_okay=False))
def train(collection_name: str, all_queries_path: str, train_queries_path: str, validation_queries_path: str,
          qrels_path: str, model_path: str):
    redis_db = redis.StrictRedis(host='localhost', port=6379, db=0)
    collection = CollectionManager(collection_name)

    print('Preparing sets...')
    documents_ids = set()
    all_text = []

    for document in collection.get_documents():
        documents_ids.add(int(document['_id']))
        all_text.append(document['text'])

    all_queries = load_cran_queries(all_queries_path)
    for query in all_queries:
        all_text.append(query)

    all_text = ' '.join(all_text)

    words_ngrams = extract_words_ngrams(all_text, 3)
    del all_text
    generate_word_vector_map(words_ngrams, redis_db)
    del words_ngrams

    training_queries = load_cran_queries(train_queries_path)
    validation_queries = load_cran_queries(validation_queries_path)

    queries_documents = {}
    qrels = load_cran_qrels(qrels_path, all_queries)
    for qrel in qrels:
        try:
            relevance_score = qrel['relevance_score']
            query = qrel['query']

            if query is not None:
                try:
                    document_id, old_relevance_score, scored_documents = queries_documents[query]
                    if relevance_score < old_relevance_score:
                        queries_documents[query] = (qrel['document_id'], relevance_score, (*scored_documents,
                                                                                           int(document_id)))
                except KeyError:
                    queries_documents[query] = (qrel['document_id'], relevance_score, ())
        except IndexError:
            pass

    training_queries_vectors: List[np.ndarray] = []
    training_positive_documents_ids: List[str] = []
    training_negative_documents_ids: List[str] = []
    for query in training_queries:
        training_queries_vectors.append(
            get_words_ngrams_vectors(query.split(' '), 7, redis_db)
        )

        positive_document_id, _, scored_documents = queries_documents[query]
        training_positive_documents_ids.append(positive_document_id)
        training_negative_documents_ids.append(np.random.choice(list(documents_ids - set(scored_documents)), 4))

    validation_queries_vectors: List[np.ndarray] = []
    validation_positive_documents_ids: List[str] = []
    validation_negative_documents_ids: List[str] = []
    for query in validation_queries:
        validation_queries_vectors.append(
            get_words_ngrams_vectors(query.split(' '), 7, redis_db)
        )

        positive_document_id, _, scored_documents = queries_documents[query]
        validation_positive_documents_ids.append(positive_document_id)
        validation_negative_documents_ids.append(np.random.choice(list(documents_ids - set(scored_documents)), 4))

    print('Training...')
    clsm = CLSMNet(model_path, redis_db, ignore_existing_model=True, window_size=7,
                   negative_documents_count=4)
    clsm.train(training_queries_vectors, training_positive_documents_ids, training_negative_documents_ids,
               validation_queries_vectors, validation_positive_documents_ids, validation_negative_documents_ids,
               collection)
    clsm.save_model()


@cli.command()
@click.argument('collection_name', type=str)
@click.argument('all_queries_path', type=click.Path(exists=True, file_okay=True, dir_okay=False))
@click.argument('test_queries_path', type=click.Path(exists=True, file_okay=True, dir_okay=False))
@click.argument('qrels_path', type=click.Path(exists=True, file_okay=True, dir_okay=False))
@click.argument('model_path', type=click.Path(exists=False, dir_okay=False))
def check(collection_name: str, all_queries_path: str, test_queries_path: str, qrels_path: str, model_path: str):
    redis_db = redis.StrictRedis(host='localhost', port=6379, db=0)
    collection = CollectionManager(collection_name)

    all_text = []
    for document in collection.get_documents():
        all_text.append(document['text'])

    queries = load_cran_queries(all_queries_path)
    for query in queries:
        all_text.append(query)

    all_text = ' '.join(all_text)

    words_ngrams = extract_words_ngrams(all_text, 3)
    generate_word_vector_map(words_ngrams, redis_db)

    queries = load_cran_queries(test_queries_path)
    queries_vectors: List[np.ndarray] = []
    for query in queries:
        queries_vectors.append(
            get_words_ngrams_vectors(query.split(' '), 7, redis_db)
        )

    clsm = CLSMNet(model_path, redis_db, negative_documents_count=4, window_size=7)
    for query_index, query_vectors in enumerate(queries_vectors):
        start = time.perf_counter()
        scores = []
        for document in collection.get_documents():
            document_vectors = get_words_ngrams_vectors(document['text'].split(' '), 7, redis_db)
            scores.append((document['_id'], clsm.get_similarity(query_vectors, document_vectors)))

        print(time.perf_counter() - start)
        print(f'Query {query_index + 1} ({queries[query_index]}): {min(scores, key=lambda x: x[1])[0]}')


@cli.command()
@click.argument('collection_name', type=str)
@click.argument('all_queries_path', type=click.Path(exists=True, file_okay=True, dir_okay=False))
@click.argument('test_queries_path', type=click.Path(exists=True, file_okay=True, dir_okay=False))
@click.argument('qrels_path', type=click.Path(exists=True, file_okay=True, dir_okay=False))
@click.argument('model_path', type=click.Path(exists=False, dir_okay=False))
@click.argument('number_of_jobs', type=int)
def check_distributed(collection_name: str, all_queries_path: str, test_queries_path: str, qrels_path: str,
                      model_path: str, number_of_jobs: int):
    redis_db = redis.StrictRedis(host='localhost', port=6379, db=0)
    collection = CollectionManager(collection_name)

    all_text = []
    for document in collection.get_documents():
        all_text.append(document['text'])

    queries = load_cran_queries(all_queries_path)
    for query in queries:
        all_text.append(query)

    all_text = ' '.join(all_text)

    words_ngrams = extract_words_ngrams(all_text, 3)
    generate_word_vector_map(words_ngrams, redis_db)

    queries = load_cran_queries(test_queries_path)
    for query_index, query in enumerate(queries):
        start = time.perf_counter()
        tasks = distribute(query, number_of_jobs, collection, model_path, window_size=7, redis_db=redis_db)
        scores = [task.get() for task in tasks]

        print(time.perf_counter() - start)
        print(f'Query {query_index + 1} ({query}): {min(scores, key=lambda x: x[1])[0]}')


if __name__ == '__main__':
    cli()
