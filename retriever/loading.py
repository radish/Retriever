import gzip
import os
import re
from subprocess import call
from typing import Iterator, Iterable, Union, Dict, List
from xml.etree import ElementTree
from xml.etree.ElementTree import Element

from retriever.cleaning import clean_text


def _parse_document(document: str):
    # We have to define some entities and replace invalid occurrences of & symbol
    document = '''<!DOCTYPE doc [
    <!ENTITY rsqb ']'>
    <!ENTITY lsqb '['>
    <!ENTITY equals '='>
    <!ENTITY plus '+'>
    ]>
    ''' + re.sub(r'(\s)&(\s)', r'\g<1>&amp;\g<2>', document)

    return ElementTree.fromstring(document)


def load_tipster_from_directory(path: Iterable[str]) -> Iterator[Element]:
    directory = os.scandir(os.path.join(path, 'AP'))
    for entry in directory:
        if entry.name.endswith('.Z'):
            # File is compressed with legacy method and must be converted to a modern format
            if call(['znew', entry.path]) != 0:
                raise SystemError(f'Converting archive format of {entry.name} failed')

            path = re.sub('\.Z$', '.gz', entry.path)
        elif entry.name.endswith('.gz'):
            # File is in a valid format
            path = entry.path
        else:
            # This aren't the documents we're looking for
            continue

        with open(path, 'rb') as archive:
            archived_file: str = gzip.decompress(archive.read()).decode('windows-1252')
            separated_documents = re.findall('(<DOC>.*?</DOC>)', archived_file, re.DOTALL)

            for document in separated_documents:
                yield _parse_document(document)


def load_cran_documents(path: str) -> Iterator[Dict[str, Union[int, str]]]:
    with open(path) as file:
        text = file.read()

    raw_documents = text.split('.I ')[1:]
    for raw_document in raw_documents:
        try:
            yield {
                '_id': re.search('\d+', raw_document)[0],
                'title': clean_text(re.search(
                    '\.T(.+)\.A', raw_document, re.MULTILINE | re.DOTALL
                ).group(1).replace('\n', ' ')).lstrip(' ').rstrip(' '),
                'text': clean_text(re.search(
                    '\.W(.+)', raw_document, re.MULTILINE | re.DOTALL
                ).group(1).replace('\n', ' ')).lstrip(' ').rstrip(' ')
            }
        except TypeError:
            pass


def load_cran_queries(path: str) -> List[str]:
    with open(path) as file:
        text = file.read()

    raw_queries = text.split('.I ')[1:]
    queries = []
    for raw_query in raw_queries:
        try:
            queries.append(clean_text(re.search(
                '\.W(.+)', raw_query, re.MULTILINE | re.DOTALL
            ).group(1).replace('\n', ' ')).lstrip(' ').rstrip(' '))
        except TypeError:
            pass

    return queries


def load_cran_qrels(path: str, all_queries: List[str]) -> Iterator[Dict[str, int]]:
    with open(path) as file:
        for line in file:
            split_line = re.sub(' +', ' ', line).split(' ')

            yield {
                'query': all_queries[int(split_line[0]) - 1],
                'document_id': split_line[1],
                'relevance_score': abs(int(split_line[2]))
            }
