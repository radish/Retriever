import pickle
from typing import List, Tuple, Dict

import numpy as np
from nltk import ngrams
from redis import StrictRedis


def extract_words_ngrams(text: str, ngram_length: int):
    split_text = text.split(' ')

    return {word: tuple(ngrams(f'#{word}#', ngram_length))
            for word in split_text}


def generate_word_vector_map(words_ngrams: Dict[str, Tuple[Tuple[str]]], redis_db: StrictRedis):
    number_of_ngrams = 0
    for word_ngrams in words_ngrams:
        number_of_ngrams += len(word_ngrams)

    ngrams_dict = {}
    index_in_vector = 0
    for word_ngrams in words_ngrams.values():
        for ngram in word_ngrams:
            if ngram not in ngrams_dict:
                ngrams_dict[ngram] = index_in_vector
                index_in_vector += 1

    total_ngrams_count = len(ngrams_dict)
    for word, word_ngrams in words_ngrams.items():
        word_vector = np.zeros((total_ngrams_count,))
        for ngram in word_ngrams:
            word_vector[ngrams_dict[ngram]] += 1

        redis_db.set(f'word_vector_{word}', pickle.dumps(word_vector))

    redis_db.set('total_ngrams_count', total_ngrams_count)


def get_words_ngrams_vectors(words: List[str], window_size: int, redis_db: StrictRedis) -> np.ndarray:
    ngrams_count = int(redis_db.get('total_ngrams_count'))

    number_of_words = len(words)
    border = (window_size - 1) // 2
    vectors = np.zeros((number_of_words, window_size * ngrams_count))
    for i in range(number_of_words):
        vector = vectors[i]
        left = i - border
        right = i + border

        position_in_vec = 0
        for d in range(left, right + 1):
            if 0 <= d < number_of_words:
                word_vector = pickle.loads(redis_db.get(f'word_vector_{words[d]}'))
                vector[position_in_vec * ngrams_count:(position_in_vec + 1) * ngrams_count] = word_vector
                vector_max = vector.max()
                if vector_max != 0:
                    vector *= 1.0 / vector.max()

            position_in_vec += 1

    return vectors
