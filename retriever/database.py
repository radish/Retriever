import json
from typing import Dict, Any, Iterator, Optional, List

import redis


class CollectionManager:
    def __init__(self, collection_name: str):
        self.name = collection_name
        self._db: redis.StrictRedis = redis.StrictRedis(host='localhost', port=6379, db=0)

    def add_document(self, document: Dict[str, Any]):
        document_id = document['_id']
        self._db.sadd(f'{self.name}_documents', document_id)
        self._db.set(f'{self.name}_document_{document_id}', json.dumps(document))

    def get_document(self, document_id: str) -> Dict[str, Any]:
        return json.loads(self._db.get(f'{self.name}_document_{document_id}'))

    def get_documents(self, ids: Optional[List[str]]=None) -> Iterator[Dict[str, Any]]:
        if ids is None:
            for document_id in self._db.sscan_iter(f'{self.name}_documents'):
                yield self.get_document(document_id.decode())
        else:
            for document_id in ids:
                yield self.get_document(document_id)

    def get_documents_ids(self):
        return [document_id.decode() for document_id in self._db.sscan_iter(f'{self.name}_documents')]
