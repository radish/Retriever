import random
from typing import List

import numpy as np
from keras import Input, backend
from keras.callbacks import ModelCheckpoint
from keras.engine import Model
from keras.layers import Convolution1D, Lambda, Dense, concatenate, Reshape, Activation
from keras.layers.merge import Dot
from keras.models import load_model
from redis import StrictRedis

from retriever.processing import get_words_ngrams_vectors


def _backend_max(x, **kwargs):
    """Overrides https://github.com/fchollet/keras/issues/4609"""
    from keras import backend  # This import is needed
    return backend.max(x, axis=1, **kwargs)


class CLSMNet:
    def __init__(self, model_path: str, redis_db: StrictRedis, ignore_existing_model: bool=False,
                 letter_ngram_size: int=3, window_size: int=3, negative_documents_count: int=4,
                 max_pooling_dimensionality: int=300, semantic_space_dimensionality: int=128, filter_length: int=1):
        self._model_path = model_path
        self._redis_db = redis_db
        self._letter_ngram_size = letter_ngram_size
        self._window_size = window_size
        self._negative_documents_count = negative_documents_count
        self._semantic_space_dimensionality = semantic_space_dimensionality
        self._max_pooling_dimensionality = max_pooling_dimensionality
        self._filter_length = filter_length

        self._ngrams_count = int(redis_db.get('total_ngrams_count'))
        self._word_depth = self._window_size * self._ngrams_count

        if ignore_existing_model is False:
            self._model: Model = load_model(model_path)
        else:
            self._model: Model = self._prepare_model()

        query = self._model.get_layer('query').output
        query_conv = self._model.get_layer('query_conv')(query)
        query_max = self._model.get_layer('query_max')(query_conv)
        query_sem = self._model.get_layer('query_sem')(query_max)

        positive_doc = self._model.get_layer('positive_doc').output
        doc_conv = self._model.get_layer('doc_conv')
        doc_max = self._model.get_layer('doc_max')
        doc_sem = self._model.get_layer('doc_sem')
        positive_doc_conv = doc_conv(positive_doc)
        positive_doc_max = doc_max(positive_doc_conv)
        positive_doc_sem = doc_sem(positive_doc_max)

        self._get_R_Q_D_p = backend.function(
            [query, positive_doc],
            [self._model.get_layer('R_Q_D_p')([query_sem, positive_doc_sem])]
        )

    def _prepare_model(self) -> Model:
        query = Input(shape=(None, self._word_depth), name='query')
        positive_doc = Input(shape=(None, self._word_depth), name='positive_doc')
        negative_docs = [Input(shape=(None, self._word_depth)) for _ in range(self._negative_documents_count)]

        query_conv = Convolution1D(
            self._max_pooling_dimensionality, self._filter_length, padding='same', input_shape=(None, self._word_depth),
            activation='tanh', name='query_conv'
        )(query)
        query_max = Lambda(_backend_max, output_shape=(self._max_pooling_dimensionality,),
                           name='query_max')(query_conv)
        query_sem = Dense(self._semantic_space_dimensionality, activation='tanh',
                          input_dim=self._max_pooling_dimensionality, name='query_sem')(query_max)

        doc_conv = Convolution1D(
            self._max_pooling_dimensionality, self._filter_length, padding='same', input_shape=(None, self._word_depth),
            activation='tanh', name='doc_conv'
        )
        doc_max = Lambda(_backend_max, output_shape=(self._max_pooling_dimensionality,),
                         name='doc_max')
        doc_sem = Dense(self._semantic_space_dimensionality, activation='tanh',
                        input_dim=self._max_pooling_dimensionality, name='doc_sem')

        positive_doc_conv = doc_conv(positive_doc)
        negative_doc_convs = [doc_conv(negative_doc) for negative_doc in negative_docs]

        positive_doc_max = doc_max(positive_doc_conv)
        negative_doc_maxes = [doc_max(neg_doc_conv) for neg_doc_conv in negative_doc_convs]

        positive_doc_sem = doc_sem(positive_doc_max)
        negative_doc_sems = [doc_sem(neg_doc_max) for neg_doc_max in negative_doc_maxes]

        R_Q_D_p = Dot(axes=1, normalize=True, name='R_Q_D_p')([query_sem, positive_doc_sem])
        R_Q_D_ns = [Dot(axes=1, normalize=True)([query_sem, neg_doc_sem]) for neg_doc_sem in
                    negative_doc_sems]

        concat_Rs = Reshape((self._negative_documents_count + 1, 1))(concatenate([R_Q_D_p] + R_Q_D_ns))

        weight = np.array([1]).reshape(1, 1, 1)
        with_gamma = Reshape((self._negative_documents_count + 1,))(
            Convolution1D(1, 1, padding='same', input_shape=(self._negative_documents_count + 1, 1),
                          activation='linear', use_bias=False, weights=[weight])(concat_Rs)
        )

        probability = Activation('softmax')(with_gamma)

        model = Model(inputs=[query, positive_doc] + negative_docs, outputs=probability)
        model.compile(optimizer='adadelta', loss='categorical_crossentropy', metrics=['accuracy'])

        return model

    def _generator(self, queries_words_vectors, collection, positive_documents_ids, negative_documents_ids):
        enumerated_queries_words_vectors = list(enumerate(queries_words_vectors))

        while True:
            random.shuffle(enumerated_queries_words_vectors)

            for i, query_words_vectors in enumerated_queries_words_vectors:
                positive_document_vectors = get_words_ngrams_vectors(
                    collection.get_document(positive_documents_ids[i])['text'].split(' '), self._window_size,
                    self._redis_db
                )

                negative_document_vectors = []
                for negative_document_id in negative_documents_ids[i]:
                    negative_document = collection.get_document(str(negative_document_id))
                    negative_document_vectors.append(
                        get_words_ngrams_vectors(negative_document['text'].split(' '), self._window_size,
                                                 self._redis_db)
                    )

                network_input = ([query_words_vectors[np.newaxis, :], positive_document_vectors[np.newaxis, :]] +
                                 [negative_document_vectors[j][np.newaxis, :]
                                  for j in range(self._negative_documents_count)])

                network_output = np.zeros(
                    self._negative_documents_count + 1
                ).reshape(1, self._negative_documents_count + 1)
                network_output[0, 0] = 1

                yield network_input, network_output

    def train(self, training_queries_words_vectors: List[np.ndarray], training_positive_documents_ids: List[str],
              training_negative_documents_ids: List[str], validation_queries_words_vectors: List[np.ndarray],
              validation_positive_documents_ids: List[str], validation_negative_documents_ids: List[str],
              collection, epochs=20):
        checkpointer = ModelCheckpoint(filepath=self._model_path, monitor='val_acc', verbose=1, save_best_only=True)
        training_generator = self._generator(training_queries_words_vectors, collection, training_positive_documents_ids,
                                             training_negative_documents_ids)
        validation_generator = self._generator(validation_queries_words_vectors, collection,
                                               validation_positive_documents_ids, validation_negative_documents_ids)

        self._model.fit_generator(training_generator, steps_per_epoch=len(training_queries_words_vectors),
                                  epochs=epochs, verbose=1, callbacks=[checkpointer],
                                  validation_data=validation_generator,
                                  validation_steps=len(validation_queries_words_vectors))

        '''for i, query_words_vectors in enumerated_queries_words_vectors:
            print(f'{i + 1}/{number_of_samples}')

            positive_document_vectors = get_words_ngrams_vectors(
                collection.get_document(positive_documents_ids[i])['text'].split(' '), self._window_size, self._redis_db
            )

            negative_document_vectors = []
            for negative_document_id in negative_documents_ids[i]:
                negative_document = collection.get_document(str(negative_document_id))
                negative_document_vectors.append(
                    get_words_ngrams_vectors(negative_document['text'].split(' '), self._window_size, self._redis_db)
                )

            print(self._model.predict(
                [query_words_vectors[np.newaxis, :], positive_document_vectors[np.newaxis, :]] +
                [negative_document_vectors[j][np.newaxis, :] for j in range(self._negative_documents_count)]
            ))'''

    def get_similarity(self, query_vectors: np.ndarray, document_vectors: np.ndarray) -> float:
        return 1 - self._get_R_Q_D_p([query_vectors[np.newaxis, :], document_vectors[np.newaxis, :]])[0][0, 0]

    def save_model(self):
        self._model.save(self._model_path)
