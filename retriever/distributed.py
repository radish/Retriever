import math
from typing import List, Sized, Tuple

import numpy as np
import redis
from celery import Celery
from celery.result import AsyncResult
from redis import StrictRedis

from retriever.database import CollectionManager
from retriever.model import CLSMNet
from retriever.processing import get_words_ngrams_vectors

app = Celery('tasks', backend='redis://localhost', broker='redis://localhost')


def chunks(iterable: Sized, chunk_size: int):
    for i in range(0, len(iterable), chunk_size):
        yield iterable[i:i + chunk_size]


def distribute(query: str, number_of_jobs: int, collection: CollectionManager, model_path: str, window_size: int,
               redis_db: StrictRedis, **kwargs) -> List[AsyncResult]:
    query_vectors = get_words_ngrams_vectors(query.split(' '), window_size, redis_db)

    documents_ids = collection.get_documents_ids()
    documents_ids_chunks = chunks(documents_ids, int(math.ceil(len(documents_ids) / number_of_jobs)))

    tasks = []
    for documents_ids_chunk in documents_ids_chunks:
        tasks.append(
            get_best_match.apply_async(
                args=[documents_ids_chunk, query_vectors.tolist(), collection.name, model_path, window_size],
                kwargs=kwargs, expires=900
            )
        )

    return tasks


@app.task
def get_best_match(documents_ids: List[str], query_vectors: List[List[float]], collection_name: str,
                   model_path: str, window_size: int, **kwargs) -> Tuple[str, float]:
    redis_db = redis.StrictRedis(host='localhost', port=6379, db=0)
    clsm = CLSMNet(model_path, redis_db, **kwargs)

    scores = []
    collection = CollectionManager(collection_name)
    for document in collection.get_documents(documents_ids):
        document_vectors = get_words_ngrams_vectors(document['text'].split(' '), window_size, redis_db)
        scores.append((document['_id'], clsm.get_similarity(np.array(query_vectors), document_vectors)))

    return min(scores, key=lambda x: x[1])
