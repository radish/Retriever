#!/usr/bin/env python3

from setuptools import setup, find_packages
from retriever import __version__

setup(
    name='Retriever',
    version=__version__,
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'Retriever = retriever.__main__:cli',
        ],
    },
    install_requires=[
        'keras',
        'h5py',
        'numpy',
        'click',
        'nltk',
        'celery',
        'redis',
    ],
    extras_require={
        'gpu': 'tensorflow-gpu',
        'cpu': 'tensorflow'
    }
)
